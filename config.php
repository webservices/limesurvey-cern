<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

## Callback function to pre-fill new user profiles with the information from SSO
if(!function_exists("hook_get_auth_webserver_profile")) {
  function hook_get_auth_webserver_profile($user_name)
  {
    return Array(
      'full_name' => $_SERVER['REDIRECT_OIDC_CLAIM_name'],
      'email' => $_SERVER['REDIRECT_OIDC_CLAIM_email'],
      'lang' => 'en',
      'htmleditormode' => 'inline');
  }
}

return array(
  'components' => array(
    'db' => array(
      'connectionString' => "mysql:host=" . getenv('DB_HOST') . ";port=" . getenv('DB_PORT') . ";dbname=" . getenv('DB_NAME') . ";",
      'emulatePrepare' => true,
      'username' => getenv('DB_USERNAME'),
      'password' => getenv('DB_PASSWORD'),
      'charset' => 'utf8mb4',
      'tablePrefix' => getenv('DB_TABLE_PREFIX'),
    ),
    'urlManager' => array(
      'urlFormat' => getenv('URL_FORMAT'),
      'rules' => array(
               // You can add your own rules here
      ),
      'showScriptName' => false,
    ),
  ),
  'config'=>array(
    #'publicurl'=>'$PUBLIC_URL',
    'debug'=>getenv("DEBUG"),
    'debugsql'=>getenv("DEBUG_SQL"),
    # Change upload directory to volume
    'uploaddir' => "/volumes/upload",
    # OIDC SSO login setting
    'auth_webserver' => true,
    // Can be usefull if you have no way to add an 'admin' user to the database
    // used by the webserver, then you could map your true loginame to admin with
    'auth_webserver_user_map' => array ('eduardoa' => 'admin'),
    'auth_webserver_autocreate_user' => true,
    'auth_webserver_autocreate_permissions' => Array(
       'surveys' => array('create'=>true),
       'participantpanel' => array('create'=>true),
       'templates' => array('read'=>true),
       ), 
  )
);
