# Source: https://hub.docker.com/r/centos/php-73-centos7
# FFI: https://github.com/sclorg/s2i-php-container/tree/master/7.3#31-to-use-your-own-setup-create-a-dockerfile-with-this-content
# Using centos/php-73-centos7 since ubi8/php-73 give authentication issues.
FROM centos/php-73-centos7
ARG version='3.25.18+210316'
ARG sha256_checksum='16c5b775ffe7cab778eb10ffe8723f45762e031ced4d254508a53f691dd9bd2d'

USER root
# Install dependencies with ROOT user
# The flexlm licensing tools require that the linux install
# includes the LSB (Linux Standard Base) libraries


RUN yum install -y epel-release && \
    yum install -y yum-plugin-priorities && \
    yum install -y https://yum.osc.edu/ondemand/1.8/ondemand-release-web-1.8-1.noarch.rpm && \
    yum update -y

RUN yum install -y rh-php73-php-gd rh-php73-php-ldap rh-php73-php-zip rh-php73-php-mbstring rh-php73-php-pdo rh-php73-php-mysqlnd sclo-php73-php-imap httpd24-mod_auth_openidc && \
    yum clean all && rm -rf /var/cache/yum/

# curl ... https://limesurvey...
# by default, you are under /opt/app/src (or similar)
ENV LIMESURVEY_VERSION=$version
RUN curl -sSL "https://github.com/LimeSurvey/LimeSurvey/archive/${version}.tar.gz" --output /tmp/limesurvey.tar.gz
RUN set -ex; \
        echo "${sha256_checksum}  /tmp/limesurvey.tar.gz" | sha256sum -c - && \
        \
        tar xzvf "/tmp/limesurvey.tar.gz" --strip-components=1 -C ./ && \
        rm -f "/tmp/limesurvey.tar.gz" 

# Custom things
COPY themes/ ./themes/

RUN mkdir -p php-pre-start/
COPY php-pre-start/ ./php-pre-start/

RUN mkdir -p httpd-cfg/
COPY httpd-cfg/ ./httpd-cfg/

USER default

# This simulates the `assemble` stage in the s2i, it means, runs composer if needed
RUN TEMPFILE=$(mktemp) && \
    curl -o "$TEMPFILE" "https://getcomposer.org/installer" && \
    php <"$TEMPFILE" && \
    ./composer.phar install --no-interaction --no-ansi --optimize-autoloader


# Add config to temporary place, will be envsubst later on runtime and moved back to
# the final folder application/config/config.php
ADD config.php ./application/config/config.php


# This simulates the `run` stage in the s2i
# Run script uses standard ways to configure the PHP application
# and execs httpd -D FOREGROUND at the end
# See more in <version>/s2i/bin/run in this repository.
# Shortly what the run script does: The httpd daemon and php needs to be
# configured, so this script prepares the configuration based on the container
# parameters (e.g. available memory) and puts the configuration files into
# the approriate places.
# This can obviously be done differently, and in that case, the final CMD
# should be set to "CMD httpd -D FOREGROUND" instead
CMD /usr/libexec/s2i/run
