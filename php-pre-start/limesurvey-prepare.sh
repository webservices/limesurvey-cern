#!/bin/bash
# Entrypoint for Docker Container

echo "Preparing LimeSurvey environment..."

if [ -f application/config/config.php ]; then
    echo 'Info: config.php already provisioned'
else
    echo 'Error: config.php not present'
    exit 1
fi




echo 'Info: Check if database already provisioned. Nevermind the Stack trace.'
php application/commands/console.php updatedb

