# Overview

This repository contains neccesary components to deploy a CERN integrated LimeSurvey instance on OpenShift.
It includes:
- The application image to be used, supporting CERN SSO and CERN Theme. Image is based on the [Custom Source-to-Image (S2I) approach](https://github.com/sclorg/s2i-php-container/tree/master/7.3#31-to-use-your-own-setup-create-a-dockerfile-with-this-content).
- A Helm template ready to be deployed on [CERN Openshift (OKD4) environment](https://paas.cern.ch).

# How to deploy your own instance

## Prerequisites

A Limesurvey instance requires two resources to be created beforehand

### Openshift application project

Needed for running the application.
In order to create it please go to [paas.cern.ch](https://paas.cern.ch) and create a new application as [documented here](https://paas.docs.cern.ch/1._Getting_Started/1-create-paas-project/).

### Database, MySQL

Limesurvey requires a mysql or postgreSQL database.

We have chosen to use the DB on demand service provided by IT and a MySQL instance.

If you don't own already a MySQL DBoD instance, please request one by using [Resources portal](https://resources.web.cern.ch/resources/Manage/DbOnDemand/Resources.aspx)

Once you have admin permissions on the Database instance, a database and db user is needed for each instance

Commands to create database and user (Please adapt names and password accordingly)
```
CREATE DATABASE `limesurvey`;
CREATE USER 'limesurveyuser'@'%' IDENTIFIED BY 'limesurveyuser_password';
GRANT ALL PRIVILEGES ON `limesurvey`.* TO 'limesurveyuser'@'%' WITH GRANT OPTION;
```

## Helm deployment 

Please check official Helm documentation for instructions on how to install helm on your machine. [Install helm](https://helm.sh/docs/intro/install/).


1. Clone this repository
2. Adapt values to your deployment. Optionally store this values file under version control.
3. Login into your PaaS namestace

```bash
oc login --server=https://api.paas.okd.cern.ch:6443
```

Select the active project where you want to deploy limesurvey

4. Install helm chart into the projects namespace

```bash
cd helm-chart/
helm install limesurvey-cern ./ --values values.yaml
```

5. Create additional resources

Create secret to store the Database user password

```yaml
kind: Secret
apiVersion: v1
metadata:
  name: limesurvey-secrets
stringData:
  DB_PASSWORD: <database_password>
type: Opaque
```



## Post deployment actions


### LimeSurvey instance initialization

Before accessing for the first time the application you need to init the database.

Connect to the application pod and follow ([CLI Installation](https://manual.limesurvey.org/Installation_using_a_command_line_interface_(CLI)). 

```
cd application/commands
php console.php install admin pwd123 Admin EMAIL@cern.ch

```

Store the admin password on some secure storage, later on we will enable SSO and this local password won't be neccesary anymore (only in case of problems)

Then you should be able to access the application and login with the account created before


### Application setup


#### Configuration -> Themes

- Install the `LimeSurvey CERN Theme` based on fruity.


#### Global Settings

##### General

- Default theme: cern_extends_fruity
- Administration theme: Bay of many
- Time difference: 1

##### Email settings

- Default site admin email: E-group or service account for the instance
- Administrator name: e-group name or service account name
- Email Method: SMTP
- SMTP host: cernmx.cern.ch:25

##### Security

`Group member can only see own group` : Off (To allow users to add Survey permissions users)


### Enable CERN SSO

In order to enable SSO there are some changes to be first configured in the application and then changes to be applied to the HELM values and deployed

#### Application - Plugin Manager - Webserver

Activate 

Key to use for username: REDIRECT_OIDC_CLAIM_cern_upn

Check to make default authentication method: Yes

#### SSO activation in HELM

Change the `ssoEnable` value

```yaml
ssoEnable: true
```

Apply changes to the deployment

```
cd helm-chart/
helm upgrade limesurvey-cern ./ --values values.yaml
```

### Expose instance to Internet

By default PaaS applications have restricted access within CERN Network area. Follow [PaaS documentation](https://paas.docs.cern.ch/5._Exposing_The_Application/2-network-visibility/) in order to open the application to Internet (**please make sure the configuration of the instance is completed and CERN SSO is enabled**)




## Known issues

### Error banner on user's first login

Due a race condition between the Login and the Account auto provission, the banner message `Incorrect username and/or password!` might be presented to the users during the first login into the instance.